package com.onlineStore.domain;

import javax.validation.constraints.*;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "users")
public class User {

	@Id
	private String id;
	
	@NotEmpty(message="The name attribute is required for the user model")
	private String name;
	
	@NotEmpty(message="The userName attribute is required for the user model")
	private String userName;
	
	@NotEmpty(message="The lastName attribute is required for the user model")
	private String lastName;
	
	@NotEmpty(message="The address attribute is required for the user model")
	private String address;
	
	@NotEmpty(message="The password attribute is required for the user model")
	private String password;
	
	@NotNull(message="The mobileNumber attribute is required for the user model")
	private long mobileNumber;
	
	@NotEmpty(message="The email attribute is required for the user model")
	@Email(message="Email pattern do not match with the standar")
	private String email;

	
	//Mongo Collection
	
//	{
//	    "_id" : ObjectId("5e7bbfb8b65e9917278f5020"),
//	    "nombre" : "Edmundo",
//	    "apellido" : "Carrillo",
//	    "direccion" : "San Angel",
//	    "curp" : "CABE921129RE"
//	}
//	
	
}
