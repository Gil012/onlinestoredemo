package com.onlineStore.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotEmpty;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.onlineStore.constant.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "products")
public class Product {

	@NotNull
	@NotEmpty// no nulo / no vacio 
	private String id;

	@NotEmpty
	private String name;

	@NotNull
	@NotEmpty// 20-140 / no vacio /no nulo
	@Size(min=20,max=140)
	private String description;

	@NotNull
	@NotEmpty
	@Pattern(regexp="^["+ApiValues.RESPRODU_PATH+ApiValues.REGEX_PRODU)// no vacio / no nulo / relative path  /resource/products/ * .png.jpge.gif 
	private String image;

	@NotNull// no nulo
	private Integer stock;
	
	@NotNull// no nulo
	private Double price;

	@Pattern(regexp=ApiValues.REGEX_PCODE)// 8 chars (Alfanumericos)
	private String code;

}
