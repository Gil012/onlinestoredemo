package com.onlineStore.api;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onlineStore.constant.ApiValues;
import com.onlineStore.domain.Product;
import com.onlineStore.service.ProductService;

@RestController
@CrossOrigin
@RequestMapping(value = ApiValues.PRODU_PATH)
public class ProductController {

	@Autowired
	private ProductService productService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Product>> retriveAllUsers() {
		List<Product> list = productService.findAll();
		return new ResponseEntity<List<Product>>(list,HttpStatus.OK); 
	}

	@GetMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Product> retriveUser(@PathVariable(value = "id") String id){
		Product prod = productService.findById(id);
		return new ResponseEntity<Product>(prod,HttpStatus.OK); 
	}
	
	@PutMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updateUser(@PathVariable(value = "id") String id,@RequestBody Product prod){
		prod.setId(id);
		productService.updateUser(prod);
		return ResponseEntity.accepted().build();
	}
	
	@PostMapping
	public ResponseEntity<String>  addUser(@RequestBody Product prod) {
		URI location = productService.addUser(prod);
		return ResponseEntity.created(location).build();
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<String>  deleteUser(@PathVariable String id){
		productService.findById(id);
		String msg = productService.deleteUser(id);
		return new ResponseEntity<String>(msg,HttpStatus.OK);
	}
}
