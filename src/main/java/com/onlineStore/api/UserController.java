package com.onlineStore.api;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onlineStore.constant.ApiValues;
import com.onlineStore.domain.User;
import com.onlineStore.model.UserDTO;
import com.onlineStore.service.UserService;

@RestController
@CrossOrigin
@RequestMapping(value = ApiValues.BASE_PATH)
public class UserController {

	@Autowired
	private UserService usuarioService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UserDTO>> retriveAllUsers() {
		List<UserDTO> list = usuarioService.findAll();
		return new ResponseEntity<List<UserDTO>>(list,HttpStatus.OK); 
	}

	@GetMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> retriveUser(@PathVariable(value = "id") String id){
		User user = usuarioService.findById(id);
		return new ResponseEntity<User>(user,HttpStatus.OK); 
	}
	
	@PutMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updateUser(@PathVariable(value = "id") String id,@Valid @RequestBody User usr){
		usr.setId(id);
		usuarioService.updateUser(usr);
		return ResponseEntity.accepted().build();
	}
	
	@PostMapping
	public ResponseEntity<String>  addUser(@Valid @RequestBody User usr) {
		URI location = usuarioService.addUser(usr);
		return ResponseEntity.created(location).build();
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<String>  deleteUser(@PathVariable String id){
		usuarioService.findById(id);
		String msg = usuarioService.deleteUser(id);
		return new ResponseEntity<String>(msg,HttpStatus.OK);
	}
}
