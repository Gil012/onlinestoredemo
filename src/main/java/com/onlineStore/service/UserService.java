package com.onlineStore.service;

import java.net.URI;
import java.util.List;

import com.onlineStore.domain.User;
import com.onlineStore.model.UserDTO;

public interface UserService {

	public List<UserDTO> findAll();

	public User findById(String id);

	public URI updateUser(User usr);

	public String deleteUser(String id);

	public URI addUser(User usr);

}
