package com.onlineStore.service;

import java.net.URI;
import java.util.List;

import com.onlineStore.domain.Product;


public interface ProductService {
	public List<Product> findAll();

	public Product findById(String id);

	public URI updateUser(Product usr);

	public String deleteUser(String id);

	public URI addUser(Product usr);
}
