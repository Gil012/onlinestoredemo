package com.onlineStore.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.onlineStore.domain.Product;

@Repository
public interface ProductRepository extends MongoRepository<Product,String> {

}
