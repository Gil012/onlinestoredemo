package com.onlineStore.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.onlineStore.domain.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

}
