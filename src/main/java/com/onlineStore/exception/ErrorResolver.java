package com.onlineStore.exception;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.onlineStore.exception.custom.ModelNotFoundException;

@ControllerAdvice
@RestController
public class ErrorResolver{

	@ExceptionHandler(ModelNotFoundException.class)
	public final ResponseEntity<ExceptionResponse> modelNotFoundExceptionHandler(ModelNotFoundException ex, WebRequest request){
		ExceptionResponse exceptionResponse = ExceptionResponse.builder()
				.timestamp(LocalDateTime.now())
				.code(ex.getMessage())
				.details(request.getDescription(false))
				.location(request.getContextPath()).build();
		return new ResponseEntity<ExceptionResponse>(exceptionResponse,HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ExceptionResponse> masterExceptionHandler(Exception ex){
		ExceptionResponse exceptionResponse = new ExceptionResponse();
		exceptionResponse.setDetails(ex.getCause().getMessage());
		exceptionResponse.setMoreInfo(ex.getClass().getName());
		exceptionResponse.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
		exceptionResponse.setTimestamp(LocalDateTime.now());
		return new ResponseEntity<ExceptionResponse>(exceptionResponse,HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(MissingPathVariableException.class)
	public final ResponseEntity<ExceptionResponse> missingPathVariableExceptionHandler(MissingPathVariableException ex, WebRequest request){
		ExceptionResponse exceptionResponse = generateExceptionResponse(ex,request,HttpStatus.INTERNAL_SERVER_ERROR,ex.getVariableName());
		return new ResponseEntity<ExceptionResponse>(exceptionResponse,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(HttpMediaTypeNotSupportedException.class)
	public final ResponseEntity<ExceptionResponse> httpMediaTypeNotSupportedExceptionHandler(HttpMediaTypeNotSupportedException ex, WebRequest request){
		StringBuffer additionalInfo = new StringBuffer("Type Supported:");
		additionalInfo.append(ex.getMessage());
		additionalInfo.append(MediaType.APPLICATION_JSON_VALUE);
		ExceptionResponse exceptionResponse = generateExceptionResponse(ex,request,HttpStatus.NOT_ACCEPTABLE,additionalInfo.toString());
		return new ResponseEntity<ExceptionResponse>(exceptionResponse,HttpStatus.NOT_ACCEPTABLE);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public final ResponseEntity<ExceptionResponse> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException ex, HttpServletRequest httpServletRequest, WebRequest request){
		StringBuffer additionalInfo = new StringBuffer();
		ex.getBindingResult().getFieldErrors().forEach(error-> additionalInfo.append(" Field:["+error.getField()+"]"));
		ExceptionResponse exceptionResponse = generateExceptionResponse(ex,request,HttpStatus.BAD_REQUEST,additionalInfo.toString());
		return new ResponseEntity<ExceptionResponse>(exceptionResponse,HttpStatus.BAD_REQUEST);
	}
	
	private ExceptionResponse generateExceptionResponse(Exception ex, WebRequest request, HttpStatus httpstatus,String additionalInfo) {
		ExceptionResponse exceptionResponse = ExceptionResponse.builder()
				.type(request.getHeader("Content-Type"))
				.code(String.valueOf(httpstatus.value()))
				.details(ex.getMessage())
				.location(request.getContextPath())
				.moreInfo(additionalInfo)
				.timestamp(LocalDateTime.now()).build();
		return exceptionResponse;
	}
}
