package com.onlineStore.exception;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionResponse {

private String type;

private String code;

private String details;

private String location;

private String moreInfo;

private LocalDateTime timestamp;

}
