package com.onlineStore.business;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.onlineStore.domain.User;
import com.onlineStore.exception.custom.ModelNotFoundException;
import com.onlineStore.model.UserDTO;
import com.onlineStore.repository.UserRepository;
import com.onlineStore.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository usuarioRepository;

	@Autowired
	private ModelMapper modelMapper;
	
	@Override
	public List<UserDTO> findAll() {
		List<User> usrs = usuarioRepository.findAll();
		List<UserDTO> usrsDTO= new ArrayList<UserDTO>();
		usrs.forEach(user -> usrsDTO.add(modelMapper.map(user,UserDTO.class)));
		return usrsDTO;
	}
	
	@Override
	public User findById(String id) {
		return usuarioRepository.findById(id).orElseThrow(
				()->new ModelNotFoundException("User not found" + id));
	}

	@Override
	public URI updateUser(User usr) {
		findById(usr.getId());
		usuarioRepository.save(usr);
		URI location = 	ServletUriComponentsBuilder
				.fromCurrentRequest().build().toUri();
		return location;
	}

	@Override
	public String deleteUser(String id) {
		findById(id);
		usuarioRepository.deleteById(id);
		return "User Deleted";		
	}

	@Override
	public URI addUser(User usr) {		
		usuarioRepository.save(usr);
		URI location = 	ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(usr.getId())
				.toUri();
		return location;
	}

}
