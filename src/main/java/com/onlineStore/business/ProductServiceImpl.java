package com.onlineStore.business;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.onlineStore.domain.Product;
import com.onlineStore.exception.custom.ModelNotFoundException;
import com.onlineStore.repository.ProductRepository;
import com.onlineStore.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public List<Product> findAll() {
		return productRepository.findAll();
	}
	
	@Override
	public Product findById(String id) {
		return productRepository.findById(id).orElseThrow(
				()->new ModelNotFoundException("User not found" + id));
	}

	@Override
	public URI updateUser(Product prod) {
		findById(prod.getId());
		productRepository.save(prod);
		URI location = 	ServletUriComponentsBuilder
				.fromCurrentRequest().build().toUri();
		return location;
	}

	@Override
	public String deleteUser(String id) {
		findById(id);
		productRepository.deleteById(id);
		return "Product Deleted";		
	}

	@Override
	public URI addUser(Product prod) {		
		productRepository.save(prod);
		URI location = 	ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(prod.getId())
				.toUri();
		return location;
	}
}
