package com.onlineStore.constant;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;

@Component
@EnableConfigurationProperties
@Getter
public class ApiValues {
	
	public static final String BASE_PATH="${constants.api.uri.basePath}";
	public static final String PRODU_PATH="${constants.api.uri.productPath}";
	public static final String RESPRODU_PATH="${constants.api.resource.productPath}";
	public static final String REGEX_PRODU="${constants.api.regex.productResource}";
	public static final String REGEX_PCODE= "${constants.api.regex.productCode}";
}
